// Copyright 2021 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	_ "path"
	"strings"

	"rsc.io/quote"
)

var (
	gitShortSHA string
	buildTime   string
)

func main() {
	handler := GetHTTPHandlers()
	host := os.Getenv("HOST")
	if host == "" {
		host = "0.0.0.0"
	}
	/* #nosec */
	http.ListenAndServe(fmt.Sprintf("%s:8080", host), &handler)
}

// GetHTTPHandlers sets up and runs the main http server
func GetHTTPHandlers() (handlers http.ServeMux) {
	handler := new(http.ServeMux)
	handler.HandleFunc("/", SayHelloHandler)
	handler.HandleFunc("/_health", HealthCheckHandler)

	return *handler
}

// SayHelloHandler handles a response
func SayHelloHandler(w http.ResponseWriter, r *http.Request) {
	var output strings.Builder

	currentEnvironment := os.Getenv("ENVIRONMENT")
	w.Header().Set("Content-Type", "text/html")

	output.WriteString(fmt.Sprintf("<html><head><title>%s :: Shift Left Security Rocks!</title></head><body>", currentEnvironment))

	output.WriteString("<h1>Welcome!</h1>")
	output.WriteString("<p>This project is used to demonstrate how to include tools and apply techniques to strengthen your CI/CD pipelines.</p>")
	output.WriteString("<p>Make a change to this file in order to simulate a new version of the application.</p>")

	output.WriteString("<h1>Hi Mike!</h1>") // This is a good line to ##_CHANGE ME_##

	output.WriteString(fmt.Sprintf("<br/><h2>Random Quote: %s</h2>", quote.Glass())) // Opt()
	output.WriteString(fmt.Sprintf("<hr/><h2>Current Environment: %s</h2>", currentEnvironment))
	output.WriteString(fmt.Sprintf("<h2>GIT Short SHA: %s</h2>", gitShortSHA))
	output.WriteString(fmt.Sprintf("<h2>Build Date/Time: %s</h2>", buildTime))
	output.WriteString("</body><html>")

	// write output to stream
	fmt.Fprintf(w, output.String())
}

// HealthCheckHandler responds with a mocked "ok" (real prod app should do some work here)
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	_, err := io.WriteString(w, `{"alive": true}`)
	if err != nil {
		// fmt.Errorf("Unable to write to reponse with error: %w", err)
		log.Fatal(err)
	}
}
